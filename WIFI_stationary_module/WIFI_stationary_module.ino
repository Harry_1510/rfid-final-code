#include "Arduino.h"
#include "HX711.h"
#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>  
#include <WiFiNINA.h> 
#include <WiFiUdp.h>
#include <RTCZero.h>
#include <ArduinoBearSSL.h>
#include <ArduinoECCX08.h>
#include <ArduinoMqttClient.h>
#include "secrets.h"

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above
#define LOADCELL_DOUT_PIN  7
#define LOADCELL_SCK_PIN  8
#define calibration_factor 170000

const char ssid[]        = SECRET_SSID;
const char pass[]        = SECRET_PASS;
const char broker[]      = SECRET_BROKER;
const char* certificate  = SECRET_CERTIFICATE;

const int buttonPin = 4;
const int buttonPin1 = 0;
const int buttonPin2 = 1;
const int buttonPin3 = 2;
const int BUTTON1 = 1;
const int BUTTON2 = 2;
const int BUTTON3 = 3;
const int BUTTON4 = 4;
const int BUTTON5 = 5;
const int BUTTON6 = 6;
const int BUTTON1LOW = 1020;
const int BUTTON1HIGH = 1040;
const int BUTTON2LOW = 50;
const int BUTTON2HIGH = 0;
const int BUTTON3LOW = 1020;
const int BUTTON3HIGH = 1040;
const int BUTTON4LOW = 50;
const int BUTTON4HIGH = 0;
const int BUTTON5LOW = 1020;
const int BUTTON5HIGH = 1040;
const int BUTTON6LOW = 50;
const int BUTTON6HIGH = 0;

int status = WL_IDLE_STATUS;

float units;

unsigned long lastMillis = 0;
unsigned long getTime() {
  // get the current time from the WiFi module  
  return WiFi.getTime();
}

LiquidCrystal_I2C lcd(0x27,16,2);
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance
HX711 scale;
WiFiClient    wifiClient;            // Used for the TCP socket connection
BearSSLClient sslClient(wifiClient); // Used for SSL/TLS connection, integrates with ECC508
MqttClient    mqttClient(sslClient);
RTCZero rtc;
 
void setup() {
  Serial.begin(9600);        // Initialize serial communications with the PC

   // while (!Serial);

  if (!ECCX08.begin()) {
    Serial.println("No ECCX08 present!");
    while (1);
  }

  // Set a callback to get the current time
  // used to validate the servers certificate
  ArduinoBearSSL.onGetTime(getTime);

  // Set the ECCX08 slot to use for the private key
  // and the accompanying public certificate for it
  sslClient.setEccSlot(0, certificate);

  // Optional, set the client id used for MQTT,
  // each device that is connected to the broker
  // must have a unique client id. The MQTTClient will generate
  // a client id for you based on the millis() value if not set
  //
  mqttClient.setId("RFID_wifi_board");
  
  // Check if the WiFi module works
  if (WiFi.status() == WL_NO_SHIELD) {
    // Wait until WiFi ready
    Serial.println("WiFi adapter not ready");
    while (true);
  }
      // Establish a WiFi connection
  while ( status != WL_CONNECTED) {
 
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
 
    // Wait 10 seconds for connection:
    delay(10000);
  }
 
  // Print connection status
  printWiFiStatus();
  
  // Start Real Time Clock
  rtc.begin();
  
  // Variable to represent epoch
  unsigned long epoch;
 
 // Variable for number of tries to NTP service
  int numberOfTries = 0, maxTries = 6;
 
 // Get epoch
  do {
    epoch = WiFi.getTime()+39600;
    numberOfTries++;
  }
 
  while ((epoch == 0) && (numberOfTries < maxTries));
 
    if (numberOfTries == maxTries) {
    Serial.print("NTP unreachable!!");
    while (1);
    }
 
    else {
    Serial.print("Epoch received: ");
    Serial.println(epoch);
    rtc.setEpoch(epoch);
    Serial.println();
    }

    if (!mqttClient.connected()) {
    // MQTT client is disconnected, connect
    connectMQTT();
  }
   
  
  lcd.init();                     
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(4,0);
   //text to print
  lcd.print("SCALE ON");
  lcd.setCursor(2,1); 
  lcd.print("SCALE ZEROED");
  delay(2000);
  lcd.clear();
  
 
  SPI.begin();               // Init SPI bus
  mfrc522.PCD_Init();        // Init MFRC522 card
  Serial.println(F("Ready To Weigh"));

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT); 
  }


  void loop(){
   if (digitalRead(buttonPin) == HIGH) {    // only goes into writing mode when button is held down
    // Writing mode:
    lcd.setCursor(0,0);
   //text to print
    lcd.print(" WRITING MODE   ");
   //set cusor position to start of next line
    lcd.setCursor(0,1);
    lcd.print(" READY TO LOAD  ");
    WRITING();
  } else {
    mqttClient.poll();                    //Keeps board connected to MQTT broker
    units = scale.get_units(), 5;         //Get values from scale

     
     lcd.setCursor(0,0);
     lcd.print("WEIGHT:    ");
     lcd.setCursor(11,0);
     lcd.print("DATE:");
     lcd.setCursor(0,1);
     lcd.print(units,5); //displays the weight in 4 decimal places only for calibration
     lcd.setCursor(5,1);
     lcd.print("KG    ");
     lcd.setCursor(11,1);
     lcd.print(String(rtc.getDay())+String("/")+String(rtc.getMonth()));
}
  }


void connectMQTT() {
  Serial.print("Attempting to connect to MQTT broker: ");
  Serial.print(broker);
  Serial.println(" ");

  while (!mqttClient.connect(broker, 8883)) {
    // failed, retry
    Serial.print(".");
    delay(5000);
  }
  Serial.println();

  Serial.println("You're connected to the MQTT broker");
  Serial.println();

  // subscribe to a topic
  mqttClient.subscribe("arduino/incoming");
}


void connectWiFi() {
  Serial.print("Attempting to connect to SSID: ");
  Serial.print(ssid);
  Serial.print(" ");

  while (WiFi.begin(ssid, pass) != WL_CONNECTED) {
    // failed, retry
    Serial.print(".");
    delay(5000);
  }
  Serial.println();

  Serial.println("You're connected to the network");
  Serial.println();
}

void printWiFiStatus() {
 
  // Print the network SSID
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  
  // Print the IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  
  // Print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void WRITING(){


// Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;


  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  
   // Code for three phase switches
   // Define analog values
   int reading1 = analogRead(buttonPin1);
   int reading2 = analogRead(buttonPin2);
   int reading3 = analogRead(buttonPin3);
   // Define initial 'buttonstates'
   int ButtonState1 = LOW;             
   int ButtonState2 = LOW;     
   int ButtonState3 = LOW;     

   //Defining what each pin reading means
    if(reading1>BUTTON1LOW && reading1<BUTTON1HIGH){
     ButtonState1 = BUTTON1;
   }else if(reading1>BUTTON2LOW){
     ButtonState1 = BUTTON2;
   }else{
     //No button is pressed;
     ButtonState1 = LOW;
   }
       if(reading2>BUTTON3LOW && reading2<BUTTON3HIGH){
     ButtonState2 = BUTTON3;
   }else if(reading2>BUTTON4LOW){
     ButtonState2 = BUTTON4;
   }else{
     //No button is pressed;
     ButtonState2 = LOW;
   }
       if(reading3>BUTTON5LOW && reading3<BUTTON5HIGH){
     ButtonState3 = BUTTON5;
   }else if(reading3>BUTTON6LOW){
     ButtonState3 = BUTTON6;
   }else{
     //No button is pressed;
     ButtonState3 = LOW;
   }
  
  //Dump UID
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  // Dump PICC type
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));

  byte buffer[34];
  byte block;
  MFRC522::StatusCode status;
  byte len;

  Serial.setTimeout(20000) ;     // wait until 20 seconds for input from serial
  // Ask for date : Date
  Serial.println(F("Load defined date"));
  String date = String(rtc.getDay())+String("/")+String(rtc.getMonth());
  date.toCharArray((char*)buffer, 30) ;
  for (byte i = 30; i < 30; i++) buffer[i] = ' ';     // pad with spaces

  
  block = 8;
  Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, buffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("DATE() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("DATE() success: "));

  block = 9;
  Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("DATE() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("DATE() success: "));
 
  // Read weight from scale: Weight
  Serial.println(F("Weight read from scale"));
  float Kg = scale.get_units(3); //define Kg as the weight of the tray
  String stringOne =  String(Kg); 
  stringOne.toCharArray((char*)buffer, 20);
  for (byte i = 20 ; i < 20; i++) buffer[i] = ' ';     // pad with spaces
  
  block = 12;
  Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, buffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("WEIGHT() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    delay(100);
    return;
  }
  else Serial.println(F("WEIGHT() success: "));

  block = 13;
  Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    delay(100);
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("WEIGHT() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    delay(100);
    return;
  }
  else Serial.println(F("WEIGHT() success: "));

   // Reading the state of switch 1
   String buf1;
  switch(ButtonState1){
     case LOW:
     Serial.println("GND");
     buf1 = "Chicken";
     buf1.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON1:
     Serial.println("5V");
     buf1 = "Waste";
     buf1.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON2:
     Serial.println("EMPTY");
     buf1 = "N/A";
     buf1.toCharArray((char*)buffer, 30) ;
     break;
   }

  for (byte i = 20; i < 20; i++) buffer[i] = ' ';     // pad with spaces
  
  block = 16;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, buffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));

  block = 17;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));

     // Reading the state of switch 2
   String buf2;
  switch(ButtonState2){
     case LOW:
     Serial.println("GND");
     buf2 = "Option 1";
     buf2.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON3:
     Serial.println("5V");
     buf2 = "Option 2";
     buf2.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON4:
     Serial.println("EMPTY");
     buf2 = "N/A";
     buf2.toCharArray((char*)buffer, 30) ;
     break;
   }

  for (byte i = 20; i < 20; i++) buffer[i] = ' ';     // pad with spaces
  
  block = 20;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, buffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));

  block = 21;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));

   // Reading the state of switch 3
   String buf3;
  switch(ButtonState3){
     case LOW:
     Serial.println("GND");
     buf3 = "Option 1";
     buf3.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON5:
     Serial.println("5V");
     buf3 = "Option 2";
     buf3.toCharArray((char*)buffer, 30) ;
     break;
     case BUTTON6:
     Serial.println("EMPTY");
     buf3 = "N/A";
     buf3.toCharArray((char*)buffer, 30) ;
     break;
   }

  for (byte i = 20; i < 20; i++) buffer[i] = ' ';     // pad with spaces
  
  block = 24;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, buffer, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));

  block = 25;
  //Serial.println(F("Authenticating using key A..."));
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Write block
  status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Write() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  else Serial.println(F("MIFARE_Write() success: "));


  Serial.println(" ");
  mfrc522.PICC_HaltA(); // Halt PICC
  mfrc522.PCD_StopCrypto1();  // Stop encryption on PCD
  lcd.clear();
  lcd.setCursor(0,0);
   //text to print
  lcd.print("DONE REMOVE TRAY");
  lcd.setCursor(0,1);
  lcd.print(Kg);
  lcd.setCursor(4,1);
  lcd.print("KG");
  lcd.setCursor(10,1);
  lcd.print(date);
   for (byte i = 0; i < mfrc522.uid.size; i++) {
  Serial.println(mfrc522.uid.uidByte[i],HEX);
  }
  Serial.println("");

  Serial.println("Publishing message");

  // send message, the Print interface can be used to set the message contents
  mqttClient.beginMessage("RFID_data_outgoing");
  mqttClient.print("WEIGHT: ");
  mqttClient.print(Kg);
  mqttClient.print("  DATE: ");
  mqttClient.print(date);
  mqttClient.print("  FEED: ");
  mqttClient.print(buf1);
  mqttClient.print("  DATA_1: ");
  mqttClient.print(buf2);
  mqttClient.print("  DATA_2: ");
  mqttClient.print(buf3);
  mqttClient.print("  TRAY: ");
   for (byte i = 0; i < mfrc522.uid.size; i++) {
  mqttClient.print(mfrc522.uid.uidByte[i],HEX);
  }
  mqttClient.endMessage();

  
  delay(5000);

}
