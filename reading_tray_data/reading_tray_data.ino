/*
 * Uses MIFARE RFID card using RFID-RC522 reader
 * Uses MFRC522 - Library
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          8             8         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/  
   



#include <LiquidCrystal_I2C.h>
#include <RTClib.h> 
#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance
LiquidCrystal_I2C lcd(0x27,16,2);
RTC_DS1307 rtc;

//*****************************************************************************************//
void setup() {
  Serial.begin(9600); 
  
#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    abort();
  }
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
   lcd.init();                     
   lcd.backlight();
   lcd.clear();
    // set cursor position to start of first line on the LCD
   lcd.setCursor(1,0);
   //text to print
   lcd.print("READY TO SCAN");
   //set cusor position to start of next line
   lcd.setCursor(1,1);
   lcd.print(" HOLD TO TAG");
    
                                          // Initialize serial communications with the PC
  SPI.begin();                                                  // Init SPI bus
  mfrc522.PCD_Init();                                              // Init MFRC522 card
  Serial.println(F("Ready to scan:"));    //shows in serial that it is ready to read
}

//*****************************************************************************************//
void loop() {


  // Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  //some variables we need
  byte block;
  byte len;
  MFRC522::StatusCode status;

  //-------------------------------------------

  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  //Serial.println(F("**Tray Detected:**"));

  //-------------------------------------------

  //mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); //dump some details about the card

  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));      //uncomment this to see all blocks in hex

  //-------------------------------------------

  Serial.print(F(" "));

  byte buffer1[18];

  block = 12;
  len = 18;

  //------------------------------------------- GET DATE
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 12, &key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
  if (status != MFRC522::STATUS_OK) {
    Serial.println(F("Failed: 1"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    lcd.clear();
 //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));  
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer1, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.println(F("Failed: 2"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    loop();
  }

  //PRINT WEIGHT
  for (uint8_t i = 0; i < 4; i++){
    Serial.write(buffer1[i]);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.write(buffer1[0]);
    lcd.write(buffer1[1]);
    lcd.write(buffer1[2]);
    lcd.write(buffer1[3]);
    lcd.print("KG");
  }
  Serial.print(" ");
  


  //---------------------------------------- GET WEIGHT

  byte buffer2[18];
  block = 8;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 8, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 1"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer2, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 2"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    loop();
  }

  //PRINT DATE
  for (uint8_t i = 0; i < 5; i++) {
    Serial.write(buffer2[i] );
    lcd.setCursor(7,0);
    lcd.write(buffer2[0]);
    lcd.write(buffer2[1]);
    lcd.write(buffer2[2]);
    lcd.write(buffer2[3]);
    lcd.write(buffer2[4]);
  }

byte buffer3[18];
  block = 16;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 16, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 1"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer3, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 2"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
 //text to print
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    loop();
  }

  //PRINT feed
  for (uint8_t i = 0; i < 10; i++) {
    Serial.write(buffer3[i] );
    lcd.setCursor(13,0);
    lcd.write(buffer3[0]);
    lcd.write(buffer3[1]);
    lcd.write(buffer3[2]);
  }

  byte buffer4[18];
  block = 20;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 20, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 1"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer4, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 2"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
 //text to print
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    loop();
  }

  //PRINT feed
  for (uint8_t i = 0; i < 10; i++) {
    Serial.write(buffer4[i] );
    lcd.setCursor(0,1);
    lcd.write(buffer4[0]);
    lcd.write(buffer4[1]);
    lcd.write(buffer4[2]);
  }

  byte buffer5[18];
  block = 24;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 24, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 1"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    //text to print
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer5, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Failed: 2"));
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("ERROR");
    lcd.setCursor(3,1);
    lcd.print("TRY AGAIN");
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
 //text to print
    lcd.print("READY TO SCAN");
 //set cusor position to start of next line
    lcd.setCursor(1,1);
    lcd.print(" HOLD TO TAG");
    Serial.println(mfrc522.GetStatusCodeName(status));
    loop();
  }

  //PRINT feed
  for (uint8_t i = 0; i < 10; i++) {
    Serial.write(buffer5[i]);
    lcd.setCursor(5,1);
    lcd.write(buffer5[0]);
    lcd.write(buffer5[1]);
    lcd.write(buffer5[2]);
  }


//Time since tray was tagged
   String a;
   String b;
   DateTime now = rtc.now();
   char buf3[] = "DD";
   b += now.toString(buf3);
  for (uint8_t i = 0; i < 2; i++) {
   a += (char)buffer2[i]; 
  }
  Serial.print(" ");
  Serial.print(b.toInt() - a.toInt());
  Serial.print(" ");
  Serial.print("DAYS OLD");

  lcd.setCursor(10,1);
  lcd.print(b.toInt() - a.toInt());
  lcd.setCursor(11,1);
  lcd.print(" DAYS");
  //----------------------------------------

  Serial.println(F(" "));


  delay(2000); //change value if you want to read cards faster
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  
 lcd.clear();
 lcd.setCursor(1,0);
 //text to print
 lcd.print("READY TO SCAN");
 //set cusor position to start of next line
 lcd.setCursor(1,1);
 lcd.print(" HOLD TO TAG");

//*****************************************************************************************//
}
